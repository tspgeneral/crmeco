package tesco.ApiRest.Model;

public class JsonFacturaMetodoPago {
	private int idFactura;
	private float montoTotal;
	private String fecha;
	private String tipo;
	private int metodoPago;
	
	public JsonFacturaMetodoPago(int idFactura, float montoTotal, String fecha, String tipo, int metodoPago) {
		super();
		this.idFactura = idFactura;
		this.montoTotal = montoTotal;
		this.fecha = fecha;
		this.tipo = tipo;
		this.metodoPago = metodoPago;
	}
	
	public int getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(int idFactura) {
		this.idFactura = idFactura;
	}
	public float getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(float montoTotal) {
		this.montoTotal = montoTotal;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getMetodoPago() {
		return metodoPago;
	}
	public void setMetodoPago(int metodoPago) {
		this.metodoPago = metodoPago;
	}

	@Override
	public String toString() {
		return "JsonFacturaMetodoPago [idFactura=" + idFactura + ", montoTotal=" + montoTotal + ", fecha=" + fecha
				+ ", tipo=" + tipo + ", metodoPago=" + metodoPago + "]";
	}
	
	
}