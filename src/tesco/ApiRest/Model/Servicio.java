/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tesco012
 */
public class Servicio {
    private final StringProperty idservicio;
    private final StringProperty nombre;
    private final StringProperty descripcion;
    private final StringProperty costo;
    private final StringProperty capacidadPersona;
    private final StringProperty tipo;
    
    public Servicio(int idservicio, String nombre, String descripcion, float costo, int capacidadPersona, String tipo) {
        
        this.idservicio = new SimpleStringProperty(String.valueOf(idservicio));
        this.nombre = new SimpleStringProperty(nombre);
        this.descripcion = new SimpleStringProperty(descripcion);
        this.costo = new SimpleStringProperty(String.valueOf(costo));
        this.capacidadPersona = new SimpleStringProperty(String.valueOf(capacidadPersona));
        this.tipo = new SimpleStringProperty(tipo);
    }
    
    
    public String getIdservicio() {
        return idservicio.get();
    }
    
    public void setIdservicio(int idservicio) {
        this.idservicio.set(String.valueOf(idservicio));
    }
    
    public String getNombre() {
        return nombre.get();
    }
    
    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }
    
    public String getDescripcion() {
        return nombre.get();
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }
    
    public String getCosto() {
        return costo.get();
    }
    
    public void setCosto(float costo) {
        this.costo.set(String.valueOf(costo));
    }
    
    public String getCapacidadPersona() {
        return capacidadPersona.get();
    }
    
    public void setCapacidadPersona(int capacidadPersona) {
        this.capacidadPersona.set(String.valueOf(capacidadPersona));
    }
    
    public String getTipo() {
        return tipo.get();
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    
    

    public StringProperty idservicioProperty() {
        return idservicio;
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public StringProperty descripcionProperty() {
        return descripcion;
    }

    public StringProperty costoProperty() {
        return costo;
    }

    public StringProperty capacidadPersonaProperty() {
        return capacidadPersona;
    }

    public StringProperty tipoProperty() {
        return tipo;
    }
    
    
}
