/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tesco012
 */
public class FacturaDato {
    private final StringProperty idFactura;
    private final StringProperty montoTotal;
    private final StringProperty fecha;
    private final StringProperty tipo;
    
    public FacturaDato(int idFactura, float montoTotal, String fecha, String tipo){
        this.idFactura =  new SimpleStringProperty(String.valueOf(idFactura));
        this.montoTotal = new SimpleStringProperty(String.valueOf(montoTotal));
        this.fecha = new SimpleStringProperty(fecha);
        this.tipo = new SimpleStringProperty(tipo);
    }
    
    public String getIdFactura() {
        return idFactura.get();
    }
    
    public void setIdReserva(int idFactura) {
        this.idFactura.set(String.valueOf(idFactura));
    }
    
    public String getMontoTotal() {
        return montoTotal.get();
    }
    
    public void setMontoTotal(float montoTotal) {
        this.montoTotal.set(String.valueOf(montoTotal));
    }

    public String getFecha() {
        return fecha.get();
    }
    
    public void setFecha(String fecha) {
        this.fecha.set(fecha);
    }
    
     public String getTipo() {
        return tipo.get();
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
     public StringProperty idFacturaProperty() {
        return idFactura;
    }

    public StringProperty montoTotalProperty() {
        return montoTotal;
    }

    public StringProperty fechaProperty() {
        return fecha;
    }

    public StringProperty tipoProperty() {
        return tipo;
    }


}
