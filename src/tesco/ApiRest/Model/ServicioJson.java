/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

/**
 *
 * @author tesco012
 */
public class ServicioJson {
    
    private int idservicio;
    private String nombre;
    private String descripcion;
    private float costo;
    private int capacidadPersona;
    private String tipo;

    public ServicioJson(int idservicio, String nombre, String descripcion, float costo, int capacidadPersona, String tipo) {
        this.idservicio = idservicio;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.costo = costo;
        this.capacidadPersona = capacidadPersona;
        this.tipo = tipo;
    }

    public int getIdservicio() {
        return idservicio;
    }

    public void setIdservicio(int idservicio) {
        this.idservicio = idservicio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public int getCapacidadPersona() {
        return capacidadPersona;
    }

    public void setCapacidadPersona(int capacidadPersona) {
        this.capacidadPersona = capacidadPersona;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "ServicioJson{" + "idservicio=" + idservicio + ", nombre=" + nombre + ", descripcion=" + descripcion + ", costo=" + costo + ", capacidadPersona=" + capacidadPersona + ", tipo=" + tipo + '}';
    }
    
    
}
