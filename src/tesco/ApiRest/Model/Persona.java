/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tesco012
 */
public class Persona {
    
    private final StringProperty DNI;
    private final StringProperty nombre;
    private final StringProperty apellido;
    private final StringProperty telefono;
    private final StringProperty pais;
    private final StringProperty provincia;
    private final StringProperty ciudad;
    private final StringProperty direccion;

    public Persona(long DNI, String nombre, String apellido, long telefono, String pais, String provincia, String ciudad, String direccion) {
        this.DNI =  new SimpleStringProperty(String.valueOf(DNI));
        this.nombre = new SimpleStringProperty(nombre);
        this.apellido = new SimpleStringProperty(apellido);
        this.telefono = new SimpleStringProperty(String.valueOf(telefono));
        this.pais = new SimpleStringProperty(pais);
        this.provincia =  new SimpleStringProperty(provincia);
        this.ciudad = new SimpleStringProperty(ciudad);
        this.direccion = new SimpleStringProperty(direccion);
    }

    public String getDNI() {
        return DNI.get();
    }
    
    public void setDni(long DNI) {
        this.DNI.set(String.valueOf(DNI));
    }
    
    public String getNombre() {
        return nombre.get();
    }
    
    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public String getApellido() {
        return apellido.get();
    }
    
    public void setApellido(String apellido) {
        this.apellido.set(apellido);
    }
    
    public String getTelefono() {
        return telefono.get();
    }

    public void setTelefono(long telefono) {
        this.telefono.set(String.valueOf(telefono));
    }

    public String getPais() {
        return pais.get();
    }
    
    public void setPais(String pais) {
        this.pais.set(pais);
    }

    public String getProvincia() {
        return provincia.get();
    }

    public void setProvincia(String provincia) {
        this.provincia.set(provincia);
    }
    
    public String getCiudad() {
        return ciudad.get();
    }
    
    public void setCiudad(String ciudad) {
        this.ciudad.set(ciudad);
    }

    public String getDireccion() {
        return direccion.get();
    }

    public void setDireccion(String direccion) {
        this.direccion.set(direccion);
    }

    public StringProperty DNIProperty() {
        return DNI;
    }

    public StringProperty nombreProperty() {
        return nombre;
    }

    public StringProperty apellidoProperty() {
        return apellido;
    }

    public StringProperty telefonoProperty() {
        return telefono;
    }

    public StringProperty paisProperty() {
        return pais;
    }

    public StringProperty provinciaProperty() {
        return provincia;
    }

    public StringProperty ciudadProperty() {
        return ciudad;
    }

    public StringProperty direccionProperty() {
        return direccion;
    }
    
    
}
