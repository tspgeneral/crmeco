/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

/**
 *
 * @author tesco012
 */
public class ReservaJson {
    private int idReserva;
	private String fechaIn;
	private String fechaOut;
	private String horaIn;
	private String horaOut;
	private int cantPersona;
	private String estado;
	private long dni;
	private int idfactura;
	private int idServicio;

    public ReservaJson(String fechaIn, String fechaOut, String horaIn, String horaOut, int cantPersona, String estado, long dni, int idfactura, int idServicio) {
        this.fechaIn = fechaIn;
        this.fechaOut = fechaOut;
        this.horaIn = horaIn;
        this.horaOut = horaOut;
        this.cantPersona = cantPersona;
        this.estado = estado;
        this.dni = dni;
        this.idfactura = idfactura;
        this.idServicio = idServicio;
    }

    public ReservaJson(int idReserva, String fechaIn, String fechaOut, String horaIn, String horaOut, int cantPersona, String estado, long dni, int idfactura, int idServicio) {
        this.idReserva = idReserva;
        this.fechaIn = fechaIn;
        this.fechaOut = fechaOut;
        this.horaIn = horaIn;
        this.horaOut = horaOut;
        this.cantPersona = cantPersona;
        this.estado = estado;
        this.dni = dni;
        this.idfactura = idfactura;
        this.idServicio = idServicio;
    }

    public int getIdReserva() {
        return idReserva;
    }

    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }

    public String getFechaIn() {
        return fechaIn;
    }

    public void setFechaIn(String fechaIn) {
        this.fechaIn = fechaIn;
    }

    public String getFechaOut() {
        return fechaOut;
    }

    public void setFechaOut(String fechaOut) {
        this.fechaOut = fechaOut;
    }

    public String getHoraIn() {
        return horaIn;
    }

    public void setHoraIn(String horaIn) {
        this.horaIn = horaIn;
    }

    public String getHoraOut() {
        return horaOut;
    }

    public void setHoraOut(String horaOut) {
        this.horaOut = horaOut;
    }

    public int getCantPersona() {
        return cantPersona;
    }

    public void setCantPersona(int cantPersona) {
        this.cantPersona = cantPersona;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public long getDni() {
        return dni;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }

    public int getIdfactura() {
        return idfactura;
    }

    public void setIdfactura(int idfactura) {
        this.idfactura = idfactura;
    }

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    @Override
    public String toString() {
        return "ReservaJson{" + "idReserva=" + idReserva + ", fechaIn=" + fechaIn + ", fechaOut=" + fechaOut + ", horaIn=" + horaIn + ", horaOut=" + horaOut + ", cantPersona=" + cantPersona + ", estado=" + estado + ", dni=" + dni + ", idfactura=" + idfactura + ", idServicio=" + idServicio + '}';
    }

    
    
}
