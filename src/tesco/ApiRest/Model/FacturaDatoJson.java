/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

/**
 *
 * @author tesco012
 */
public class FacturaDatoJson {
    
    private int idFactura;
    private float montoTotal;
    private String fecha;
    private String tipo;

    public FacturaDatoJson(int idFactura, float montoTotal, String fecha, String tipo) {
        this.idFactura = idFactura;
        this.montoTotal = montoTotal;
        this.fecha = fecha;
        this.tipo = tipo;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public float getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(float montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "FacturaDatoJson{" + "idFactura=" + idFactura + ", montoTotal=" + montoTotal + ", fecha=" + fecha + ", tipo=" + tipo + '}';
    }
    
    
}
