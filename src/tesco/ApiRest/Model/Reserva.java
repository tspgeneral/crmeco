/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tesco012
 */
public class Reserva {
    
    private final StringProperty idReserva;
    private final StringProperty fechaIn;
    private final StringProperty fechaOut;
    private final StringProperty horaIn;
    private final StringProperty horaOut;
    private final StringProperty cantPersona;
    private final StringProperty estado;
    private final StringProperty dni;
    private final StringProperty idfactura;
    private final StringProperty idServicio;

    public Reserva(int idReserva, String fechaIn, String fechaOut, String horaIn, String horaOut, int cantPersona,String estado, long dni, int idfactura, int idServicio) {
        this.idReserva =  new SimpleStringProperty(String.valueOf(idReserva));
        this.fechaIn = new SimpleStringProperty(fechaIn);
        this.fechaOut = new SimpleStringProperty(fechaOut);
        this.horaIn = new SimpleStringProperty(horaIn);
        this.horaOut = new SimpleStringProperty(horaOut);
        this.cantPersona = new SimpleStringProperty(String.valueOf(cantPersona));
        this.estado = new SimpleStringProperty(estado);
        this.dni = new SimpleStringProperty(String.valueOf(dni));
        this.idfactura = new SimpleStringProperty(String.valueOf(idfactura));
        this.idServicio = new SimpleStringProperty(String.valueOf(idServicio));
    }

    public String getIdReserva() {
        return idReserva.get();
    }
    
    public void setIdReserva(long idReserva) {
        this.idReserva.set(String.valueOf(idReserva));
    }
    
    public String getFechaIn() {
        return fechaIn.get();
    }
    
    public void setFechaIn(String fechaIn) {
        this.fechaIn.set(fechaIn);
    }

    public String getFechaOut() {
        return fechaOut.get();
    }
    
    public void setFechaOut(String fechaOut) {
        this.fechaOut.set(fechaOut);
    }
    
    public String getHoraIn() {
        return horaIn.get();
    }

    public void setHoraIn(String horaIn) {
        this.horaIn.set(horaIn);
    }

    public String getHoraOut() {
        return horaOut.get();
    }
    
    public void setHoraOut(String horaOut) {
        this.horaOut.set(horaOut);
    }

    public String getCantPersona() {
        return cantPersona.get();
    }

    public void setCantPersona(int cantPersona) {
        this.cantPersona.set(String.valueOf(cantPersona));
    }
    
    public String getEstado() {
        return estado.get();
    }
    
    public void setEstado(String estado) {
        this.estado.set(estado);
    }

    public String getDni() {
        return dni.get();
    }

    public void setDni(long dni) {
        this.dni.set(String.valueOf(dni));
    }
    
    public String getIdfactura() {
        return idfactura.get();
    }

    public void setIdfactura(int idfacturadni) {
        this.idfactura.set(String.valueOf(idfactura));
    }
    
    public String getIdServicio() {
        return idServicio.get();
    }

    public void setIdServicio(int idServicio) {
        this.idServicio.set(String.valueOf(idServicio));
    }

    public StringProperty idReservaProperty() {
        return idReserva;
    }

    public StringProperty fechaInProperty() {
        return fechaIn;
    }

    public StringProperty fechaOutProperty() {
        return fechaOut;
    }

    public StringProperty horaInProperty() {
        return horaIn;
    }

    public StringProperty horaOutProperty() {
        return horaOut;
    }

    public StringProperty cantPersonaProperty() {
        return cantPersona;
    }

    public StringProperty estadoProperty() {
        return estado;
    }

    public StringProperty dniProperty() {
        return dni;
    }
    
    public StringProperty idfacturaProperty() {
        return idfactura;
    }
    
    public StringProperty idServicioProperty() {
        return idServicio;
    } 
}
