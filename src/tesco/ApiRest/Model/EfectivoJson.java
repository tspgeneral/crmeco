/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

/**
 *
 * @author tesco012
 */
public class EfectivoJson {

    private int idEfectivo;
    private String moneda;

    public EfectivoJson(String moneda) {
        this.moneda = moneda;
    }

    public int getIdEfectivo() {
        return idEfectivo;
    }

    public void setIdEfectivo(int idEfectivo) {
        this.idEfectivo = idEfectivo;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    @Override
    public String toString() {
        return "EfectivoJson{" + "idEfectivo=" + idEfectivo + ", moneda=" + moneda + '}';
    }
    
    

}
