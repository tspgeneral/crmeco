/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

/**
 *
 * @author tesco012
 */
public class UsersJson {
    private String usuarioJS;
    private String passJS;
    private String emailJS;
    private String rolesJS;

    public String getUsuarioJS() {
        return usuarioJS;
    }

    public void setUsuarioJS(String usuarioJS) {
        this.usuarioJS = usuarioJS;
    }

    public String getPassJS() {
        return passJS;
    }

    public void setPassJS(String passJS) {
        this.passJS = passJS;
    }

    public String getEmailJS() {
        return emailJS;
    }

    public void setEmailJS(String emailJS) {
        this.emailJS = emailJS;
    }

    public String getRolesJS() {
        return rolesJS;
    }

    public void setRolesJS(String rolesJS) {
        this.rolesJS = rolesJS;
    }

    public UsersJson(String usuarioJS, String passJS, String emailJS, String rolesJS) {
        this.usuarioJS = usuarioJS;
        this.passJS = passJS;
        this.emailJS = emailJS;
        this.rolesJS = rolesJS;
    }

    @Override
    public String toString() {
        return "UsersJson{" + "usuarioJS=" + usuarioJS + ", passJS=" + passJS + ", emailJS=" + emailJS + ", rolesJS=" + rolesJS + '}';
    }

    
   
    
}
