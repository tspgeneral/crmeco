package tesco.ApiRest.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FacturaMetodoPago {
    private final StringProperty idFactura;
    private final StringProperty montoTotal;
    private final StringProperty fecha;
    private final StringProperty tipo;
    private final StringProperty metodoPago;
    
    public FacturaMetodoPago(int idFactura, float montoTotal, String fecha, String tipo, int metodoPago ){
        this.idFactura =  new SimpleStringProperty(String.valueOf(idFactura));
        this.montoTotal = new SimpleStringProperty(String.valueOf(montoTotal));
        this.fecha = new SimpleStringProperty(fecha);
        this.tipo = new SimpleStringProperty(tipo);
        this.metodoPago =  new SimpleStringProperty(String.valueOf(metodoPago));
    }
    
    public String getIdFactura() {
        return idFactura.get();
    }
    
    public void setIdReserva(int idFactura) {
        this.idFactura.set(String.valueOf(idFactura));
    }
    
    public String getMontoTotal() {
        return montoTotal.get();
    }
    
    public void setMontoTotal(float montoTotal) {
        this.montoTotal.set(String.valueOf(montoTotal));
    }

    public String getFecha() {
        return fecha.get();
    }
    
    public void setFecha(String fecha) {
        this.fecha.set(fecha);
    }
    
    public String getTipo() {
        return tipo.get();
    }
    
    public void setTipo(String tipo) {
        this.tipo.set(tipo);
    }
    
    public String getMetodoPago() {
        return metodoPago.get();
    }
    
    public void setMetodoPago(String metodoPago) {
        this.metodoPago.set(metodoPago);
    }
    
     public StringProperty idFacturaProperty() {
        return idFactura;
    }

    public StringProperty montoTotalProperty() {
        return montoTotal;
    }

    public StringProperty fechaProperty() {
        return fecha;
    }

    public StringProperty tipoProperty() {
        return tipo;
    }
    
    public StringProperty metodoPagoProperty() {
        return metodoPago;
    }
    
}