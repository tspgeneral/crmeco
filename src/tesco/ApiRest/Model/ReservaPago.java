/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest.Model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author tesco012
 */
public class ReservaPago {
    
    private final StringProperty idReserva;
    private final StringProperty fechaIn;
    private final StringProperty fechaOut;
    private final StringProperty horaIn;
    private final StringProperty horaOut;
    private final StringProperty idServicio;
    private final StringProperty servicioNombre;
    private final StringProperty servicioCosto;

    public ReservaPago(int idReserva, String fechaIn, String fechaOut, String horaIn, String horaOut, int idServicio,String servicioNombre, float servicioCosto ) {
        this.idReserva =  new SimpleStringProperty(String.valueOf(idReserva));
        this.fechaIn = new SimpleStringProperty(fechaIn);
        this.fechaOut = new SimpleStringProperty(fechaOut);
        this.horaIn = new SimpleStringProperty(horaIn);
        this.horaOut = new SimpleStringProperty(horaOut);
        this.idServicio = new SimpleStringProperty(String.valueOf(idServicio));
        this.servicioNombre =  new SimpleStringProperty(servicioNombre);
        this.servicioCosto = new SimpleStringProperty(String.valueOf(servicioCosto));
    }

    public String getIdReserva() {
        return idReserva.get();
    }
    
    public void setIdReserva(long idReserva) {
        this.idReserva.set(String.valueOf(idReserva));
    }
    
    public String getFechaIn() {
        return fechaIn.get();
    }
    
    public void setFechaIn(String fechaIn) {
        this.fechaIn.set(fechaIn);
    }

    public String getFechaOut() {
        return fechaOut.get();
    }
    
    public void setFechaOut(String fechaOut) {
        this.fechaOut.set(fechaOut);
    }
    
    public String getHoraIn() {
        return horaIn.get();
    }

    public void setHoraIn(String horaIn) {
        this.horaIn.set(horaIn);
    }

    public String getHoraOut() {
        return horaOut.get();
    }
    
    public void setHoraOut(String horaOut) {
        this.horaOut.set(horaOut);
    }

    public String getIdServicio() {
        return idServicio.get();
    }

    public void setIdServicio(int idServicio) {
        this.idServicio.set(String.valueOf(idServicio));
    }
    
    public String getServicioNombre() {
        return servicioNombre.get();
    }
    
    public void setServicioNombre(String servicioNombre) {
        this.servicioNombre.set(servicioNombre);
    }
    
    public String getServicioCosto() {
        return idReserva.get();
    }
    
    public void setServicioCosto(float servicioCosto) {
        this.servicioCosto.set(String.valueOf(servicioCosto));
    }
    
    public StringProperty idReservaProperty() {
        return idReserva;
    }

    public StringProperty fechaInProperty() {
        return fechaIn;
    }

    public StringProperty fechaOutProperty() {
        return fechaOut;
    }

    public StringProperty horaInProperty() {
        return horaIn;
    }

    public StringProperty horaOutProperty() {
        return horaOut;
    } 
    
    public StringProperty idServicioProperty() {
        return idServicio;
    } 
    
    public StringProperty servicioNombreProperty() {
        return servicioNombre;
    } 
    
    public StringProperty servicioCostooProperty() {
        return servicioCosto;
    } 
}
