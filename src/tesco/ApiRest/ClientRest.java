/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.ApiRest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import tesco.ApiRest.Model.EfectivoJson;
import tesco.ApiRest.Model.FacturaDatoJson;
import tesco.ApiRest.Model.FacturaJson;
import tesco.ApiRest.Model.JsonFacturaMetodoPago;
import tesco.ApiRest.Model.PersonJson;
import tesco.ApiRest.Model.ReservaJson;
import tesco.ApiRest.Model.ReservaPagoJson;
import tesco.ApiRest.Model.ServicioJson;
import tesco.ApiRest.Model.UsersJson;

/**
 *
 * @author tesco012
 */
public class ClientRest {
    //"http://tesco.s0ytes.net:8080/apiservermsl/"
    //"http://ecoturismo.herokuapp.com/"
    private static final String serverApi =  "http://ecoturismo.herokuapp.com/"; 
    //"http://localhost:8080/apiservermsl/"
    public static List<JsonFacturaMetodoPago> getFacturasMetodoPago() {
        try {
            URL url = new URL( serverApi + "facturas" );
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            List<JsonFacturaMetodoPago> listFacturasMetodoPago = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                listFacturasMetodoPago = gson.fromJson(output, new TypeToken<List<JsonFacturaMetodoPago>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            listFacturasMetodoPago.forEach(x -> System.out.println(x));
            return listFacturasMetodoPago;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    public static List<ReservaJson> getReservas() {
        try {
            URL url = new URL(serverApi + "reservas");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            List<ReservaJson> listReservas = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                listReservas = gson.fromJson(output, new TypeToken<List<ReservaJson>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            listReservas.forEach(x -> System.out.println(x));
            return listReservas;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
    public static List<FacturaDatoJson> getFacturasFecha(String fecha) {
        try {
            URL url = new URL( serverApi + "facturas/" + fecha);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            List<FacturaDatoJson> listFacturas = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                listFacturas = gson.fromJson(output, new TypeToken<List<FacturaDatoJson>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            listFacturas.forEach(x -> System.out.println(x));
            return listFacturas;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
    public static boolean getReservaPagada(String idReserva, String idFactura) {
        try {
            URL url = new URL(serverApi +"reservapagada/" + idReserva + "/"+idFactura);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            conn.disconnect();
            return true;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return false;
    }
    
    public static FacturaJson postPagoAdd(FacturaJson factura) {
        try {

            URL url = new URL(serverApi + "factura");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String input = gson.toJson(factura);
            System.out.println(input);

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != 200) { //HttpURLConnection.HTTP_CREATED
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            FacturaJson facturaJ = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gsonO = new Gson();
                facturaJ = gsonO.fromJson(output, FacturaJson.class);
            }

            conn.disconnect();
            System.out.println(facturaJ);
            return facturaJ;

        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
            return null;

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }
    
    public static List<EfectivoJson> getEfectivo() {
        try {
            URL url = new URL(serverApi +"efectivos");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            List<EfectivoJson> listEfectivos = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                listEfectivos = gson.fromJson(output, new TypeToken<List<EfectivoJson>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            listEfectivos.forEach(x -> System.out.println(x));
            return listEfectivos;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    public static List<ReservaPagoJson> getReservaPagoDNI(String dni) {
        try {
            URL url = new URL(serverApi + "reservas/" + dni);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            List<ReservaPagoJson> listReservasPago = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                listReservasPago = gson.fromJson(output, new TypeToken<List<ReservaPagoJson>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            listReservasPago.forEach(x -> System.out.println(x));
            return listReservasPago;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static boolean getReservaConfirma(String idReserva) {
        try {
            URL url = new URL(serverApi + "reservaconfirma/" + idReserva);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            conn.disconnect();
            return true;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return false;
    }

    public static List<ReservaJson> getReservaDNI(String dniRes, String estado) {
        try {
            URL url = new URL(serverApi + "reservas/" + dniRes + "/" + estado);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            List<ReservaJson> listReservas = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                listReservas = gson.fromJson(output, new TypeToken<List<ReservaJson>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            listReservas.forEach(x -> System.out.println(x));
            return listReservas;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static boolean postReservasAdd(ReservaJson reserva) {
        try {

            URL url = new URL(serverApi + "reserva");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String input = gson.toJson(reserva);
            System.out.println(input);

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != 200) { //HttpURLConnection.HTTP_CREATED
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();
            return true;

        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
            return false;

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return false;
        }
    }

    public static List<ServicioJson> getServicios() {

        try {
            //http://api.openweathermap.org/data/2.5/weather?q=London&APPID=bdf3684b98da7bff842c25dab4f82c65
            //http://jsonplaceholder.typicode.com/users
            URL url = new URL(serverApi + "servicios");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            List<ServicioJson> listServicios = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                listServicios = gson.fromJson(output, new TypeToken<List<ServicioJson>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            listServicios.forEach(x -> System.out.println(x));
            return listServicios;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static boolean postClienteAdd(PersonJson persona) {
        try {

            URL url = new URL(serverApi +"persona");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            Gson gson = new Gson();
            String input = gson.toJson(persona);
            System.out.println(input);

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != 200) { //HttpURLConnection.HTTP_CREATED
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();
            return true;

        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
            return false;

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return false;
        }
    }

    public static PersonJson getPersonaDNI(long DNI) {
        try {
            String idDNI = String.valueOf(DNI);
            URL url = new URL(serverApi + "persona/" + idDNI);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            PersonJson person = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                person = gson.fromJson(output, PersonJson.class);
            }
            conn.disconnect();
            System.out.println(person);
            return person;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static List<UsersJson> getUsuario() {

        try {
            //http://api.openweathermap.org/data/2.5/weather?q=London&APPID=bdf3684b98da7bff842c25dab4f82c65
            //http://jsonplaceholder.typicode.com/users
            URL url = new URL(serverApi + "usuarios");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            List<UsersJson> list = null;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                Gson gson = new Gson();
                list = gson.fromJson(output, new TypeToken<List<UsersJson>>() {
                }.getType());
            }
            //String json = "[{\"usuarioJS\":\"admin\",\"passJS\":\"admin\",\"emailJS\":\"admin@hotmail.com\"},{\"usuarioJS\":\"test\",\"passJS\":\"test\",\"emailJS\":\"test@gmail.com\"}]";
            conn.disconnect();
            list.forEach(x -> System.out.println(x));
            return list;
        } catch (MalformedURLException ex) {
            System.err.println(ex.getMessage());
        } catch (IOException | RuntimeException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    /*
     /////////////////////////
     //ALERTA: Los metodos anterios no funcionan ya que java fx tiene problema con los hilos.
     /////////////////////////
     public static void getRest2() {
     Client  client = Client.create();
     WebResource webResource = client.resource("http://192.168.0.7:8080/apiservermsl/usuarios");
     ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
     //a successful response returns 200
     if (response.getStatus() != 200) {
     throw new RuntimeException("HTTP Error: " + response.getStatus());
     }
     String result = response.getEntity(String.class);
     System.out.println("Response from the Server: ");
     System.out.println(result);

     }

     public static void getRest3() {

     // register genson in jersey client
     ClientConfig cfg = new DefaultClientConfig(GensonJsonConverter.class);
     Client client = Client.create(cfg);
     WebResource webResource = client.resource("http://192.168.0.7:8080/apiservermsl/usuarios");

     // you can map it to a pojo, no need to have a string or map
     UsersJson pojo = webResource
     .accept(MediaType.APPLICATION_JSON)
     .get(UsersJson.class);

     }
     */
}
