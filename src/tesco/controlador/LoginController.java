/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.controlador;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import tesco.ApiRest.ClientRest;
import tesco.ApiRest.Model.UsersJson;

/**
 * FXML Controller class
 *
 * @author tesco012
 */
public class LoginController implements Initializable {

    @FXML
    private Button buttonLogin;
    @FXML
    private Button buttonLoginExit;
    @FXML
    private TextField textLoginUsuario;
    @FXML
    private PasswordField textLoginPass;
    @FXML
    private Label labelLoginError;

    @FXML
    private void buttonLoginAction(ActionEvent event) throws IOException {
        buttonLogin.setDisable(true);
        System.out.println(textLoginPass.getText());
        List<UsersJson> lisUser = ClientRest.getUsuario();
        for (UsersJson userP : lisUser) {
            if (textLoginUsuario.getText().equals(userP.getUsuarioJS()) && textLoginPass.getText().equals(userP.getPassJS())) {
                ((Node) (event.getSource())).getScene().getWindow().hide();
                if (userP.getRolesJS().equals("ADMIN")) {
                    Parent parent = FXMLLoader.load(getClass().getResource("/tesco/vista/admin.fxml"));
                    Stage stage = new Stage();
                    Scene scene = new Scene(parent);
                    stage.setScene(scene);
                    stage.setTitle("CRM Ecoturisamo");
                    Image ico = new Image("/recursos/Icon.png");
                    stage.getIcons().add(ico);
                    stage.show();
                } else {
                    Parent parent = FXMLLoader.load(getClass().getResource("/tesco/vista/Main.fxml"));
                    Stage stage = new Stage();
                    Scene scene = new Scene(parent);
                    stage.setScene(scene);
                    stage.setTitle("CRM Ecoturisamo");
                    Image ico = new Image("/recursos/Icon.png");
                    stage.getIcons().add(ico);
                    stage.show();
                }

            } else {
                labelLoginError.setText("Usuario o contrase�a Incorrecto");
            }
        }

        buttonLogin.setDisable(false);
    }

    @FXML
    private void buttonLoginExitAction(ActionEvent event) {
        System.exit(0);
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
