/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.controlador;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Slider;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tesco.ApiRest.ClientRest;
import tesco.ApiRest.Model.EfectivoJson;
import tesco.ApiRest.Model.FacturaDato;
import tesco.ApiRest.Model.FacturaDatoJson;
import tesco.ApiRest.Model.FacturaJson;
import tesco.ApiRest.Model.PersonJson;
import tesco.ApiRest.Model.Persona;
import tesco.ApiRest.Model.Reserva;
import tesco.ApiRest.Model.ReservaJson;
import tesco.ApiRest.Model.ReservaPago;
import tesco.ApiRest.Model.ReservaPagoJson;
import tesco.ApiRest.Model.Servicio;
import tesco.ApiRest.Model.ServicioJson;

/**
 *
 * @author tesco012
 */
public class MainController implements Initializable {

    final WebView browser = new WebView();
    final WebEngine webEngine = browser.getEngine();
    private static Alert alert;
    private ObservableList<Persona> personsData = FXCollections.observableArrayList();
    private ObservableList<Servicio> serviciosData = FXCollections.observableArrayList();
    private ObservableList<Reserva> reservasData = FXCollections.observableArrayList();
    private ObservableList<ReservaPago> reservaPagoData = FXCollections.observableArrayList();
    private ObservableList<FacturaDato> facturasData = FXCollections.observableArrayList();
    private Persona cliente;
    @FXML
    private TableView<Servicio> tableServicios;
    @FXML
    private TableColumn<Servicio, String> columnIdservicioS;
    @FXML
    private TableColumn<Servicio, String> columnNombreS;
    @FXML
    private TableColumn<Servicio, String> columnDescripcionS;
    @FXML
    private TableColumn<Servicio, String> columnCostoS;
    @FXML
    private TableColumn<Servicio, String> columnCapacidadPersonaS;
    @FXML
    private TableColumn<Servicio, String> columnTipoS;

    @FXML
    private TableView<Persona> tableCliente;
    @FXML
    private TableColumn<Persona, String> columnDNI;
    @FXML
    private TableColumn<Persona, String> columnNombre;
    @FXML
    private TableColumn<Persona, String> columnApellido;
    @FXML
    private TableColumn<Persona, String> columnTelefono;
    @FXML
    private TableColumn<Persona, String> columnProvincia;
    @FXML
    private TableColumn<Persona, String> columnCiudad;
    @FXML
    private TableColumn<Persona, String> columnDireccion;
    //Buscar cliente controles
    @FXML
    private Button buttonPersonDNI;
    @FXML
    private Button buttonBuscaClienteLimpia;
    @FXML
    private TextField textPersonDNI;
    @FXML
    private TextField textPersonNombre;
    @FXML
    private TextField textPersonApellido;
    //ABM cliente controles
    @FXML
    private TextField textClienteDNI;
    @FXML
    private TextField textClienteNombre;
    @FXML
    private TextField textClienteApellido;
    @FXML
    private TextField textClienteTelefono;
    @FXML
    private TextField textClientePais;
    @FXML
    private TextField textClienteProvincia;
    @FXML
    private TextField textClienteCuidad;
    @FXML
    private TextField textClienteDireccion;
    @FXML
    private Button buttonClienteElimina;
    @FXML
    private Button buttonClienteModifica;
    @FXML
    private Button buttonClienteAgregar;
    //alta reserva
    @FXML
    private TextField textClienteReserva;
    @FXML
    private TextField textCanPerReserva;
    @FXML
    private TextField textServicioReserva;
    @FXML
    private DatePicker fechaInReserva;
    @FXML
    private DatePicker fechaOutReserva;
    @FXML
    private Slider horaInReserva;
    @FXML
    private Slider horaOutReserva;
    @FXML
    private ComboBox<String> confirmaReserva;
    @FXML
    private Button buttonReservaAdd;
    //ABM CONFIRMACION
    @FXML
    private TableView<Reserva> tableReservas;
    @FXML
    private TableColumn<Reserva, String> columnReservaId;
    @FXML
    private TableColumn<Reserva, String> columnReservaServicio;
    @FXML
    private TableColumn<Reserva, String> columnReservaFechaIn;
    @FXML
    private TableColumn<Reserva, String> columnReservaHoraIn;
    @FXML
    private TableColumn<Reserva, String> columnReservaEstado;
    @FXML
    private TextField textConfirmaId;
    @FXML
    private TextField textConfirmaFecha;
    @FXML
    private Button buttonConfirma;
    //cobro factura
    @FXML
    private TableView<ReservaPago> tableReservasPago;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoIdReserva;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoFechaIn;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoFechaOut;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoHoraIn;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoHoraOut;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoServicio;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoCosto;

    @FXML
    private TextField costoTotal;
    @FXML
    private ComboBox<String> tipoFactura;
    @FXML
    private ComboBox<String> monedaCombo;
    @FXML
    private TextField textCobroCBU;
    @FXML
    private TextField textCobroBanco;
    @FXML
    private TextField textCobroTitular;
    @FXML
    private TextField textCobroNumTargeta;
    @FXML
    private DatePicker fechaCobroVencimiento;
    @FXML
    private PasswordField textCobroCodSeg;
    @FXML
    private TextField textCobroNumCuotas;
    @FXML
    private Button buttonPagar;

    //control Ganancia
    @FXML
    private TableView<FacturaDato> tableGanancia;
    @FXML
    private TableColumn<FacturaDato, String> columnGananciaIdFactura;
    @FXML
    private TableColumn<FacturaDato, String> columnGananciaMontoTotal;
    @FXML
    private TableColumn<FacturaDato, String> columnGananciaFecha;
    @FXML
    private TableColumn<FacturaDato, String> columnGananciaTipo;
    @FXML
    private Button buttonGananciaFF;
    @FXML
    private DatePicker fechaGanancia;
    @FXML
    private TextField textGananciaTotal;
    @FXML
    private Hyperlink Hbusqueda;
    @FXML
    private Hyperlink Hseleccion;
    @FXML
    private Hyperlink Hreserva;
    @FXML
    private Hyperlink Hconfirmacion;
    @FXML
    private Hyperlink Hcobro;

    //Escucha de las acciones de los controles
    @FXML
    private void HcobroAction(ActionEvent event) {
        Stage stage = new Stage();
        WebView webview = new WebView();
        webview.getEngine().load(
                MainController.class.getResource("/recursos/Manual/Cobro.html").toExternalForm()
        );
        stage.setScene(new Scene(webview));
        stage.show();
    }
    @FXML
    private void HbusquedaAction(ActionEvent event) {
        //ClassLoader classLoader = getClass().getClassLoader();
        //File file = new File(classLoader.getResource("/recursos/Manual/BuscarCliente.html").getFile());
        Stage stage = new Stage();
        WebView webview = new WebView();
        webview.getEngine().load(
                MainController.class.getResource("/recursos/Manual/BuscarCliente.html").toExternalForm()
        );
        stage.setScene(new Scene(webview));
        stage.show();
    }
    
    @FXML
    private void HseleccionAction(ActionEvent event) {
        //ClassLoader classLoader = getClass().getClassLoader();
        //File file = new File(classLoader.getResource("/recursos/Manual/BuscarCliente.html").getFile());
        Stage stage = new Stage();
        WebView webview = new WebView();
        webview.getEngine().load(
                MainController.class.getResource("/recursos/Manual/ManejoDeCliente.html").toExternalForm()
        );
        stage.setScene(new Scene(webview));
        stage.show();
    }
    @FXML
    private void HreservaAction(ActionEvent event) {
        //ClassLoader classLoader = getClass().getClassLoader();
        //File file = new File(classLoader.getResource("/recursos/Manual/BuscarCliente.html").getFile());
        Stage stage = new Stage();
        WebView webview = new WebView();
        webview.getEngine().load(
                MainController.class.getResource("/recursos/Manual/Reserva.html").toExternalForm()
        );
        stage.setScene(new Scene(webview));
        stage.show();
    }
    @FXML
    private void HconfirmacionAction(ActionEvent event) {
        //ClassLoader classLoader = getClass().getClassLoader();
        //File file = new File(classLoader.getResource("/recursos/Manual/BuscarCliente.html").getFile());
        Stage stage = new Stage();
        WebView webview = new WebView();
        webview.getEngine().load(
                MainController.class.getResource("/recursos/Manual/Confirmacion.html").toExternalForm()
        );
        stage.setScene(new Scene(webview));
        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // Initialize the person table 
            columnDNI.setCellValueFactory(cellData -> cellData.getValue().DNIProperty());
            columnNombre.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());
            columnApellido.setCellValueFactory(cellData -> cellData.getValue().apellidoProperty());
            columnTelefono.setCellValueFactory(cellData -> cellData.getValue().telefonoProperty());
            columnProvincia.setCellValueFactory(cellData -> cellData.getValue().provinciaProperty());
            columnCiudad.setCellValueFactory(cellData -> cellData.getValue().ciudadProperty());
            columnDireccion.setCellValueFactory(cellData -> cellData.getValue().direccionProperty());
            // Clear person details.
            showCustomerDetails(null);
            // Listen for selection changes and show the person details when changed.
            tableCliente.getSelectionModel().selectedItemProperty().addListener(
                    (observable, oldValue, newValue) -> showCustomerDetails(newValue));

            columnIdservicioS.setCellValueFactory(cellData -> cellData.getValue().idservicioProperty());
            columnNombreS.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());
            columnDescripcionS.setCellValueFactory(cellData -> cellData.getValue().descripcionProperty());
            columnCostoS.setCellValueFactory(cellData -> cellData.getValue().costoProperty());
            columnCapacidadPersonaS.setCellValueFactory(cellData -> cellData.getValue().capacidadPersonaProperty());
            columnTipoS.setCellValueFactory(cellData -> cellData.getValue().tipoProperty());
            showServiceDetails(null);
            // Listen for selection changes and show the person details when changed.
            tableServicios.getSelectionModel().selectedItemProperty().addListener(
                    (observable, oldValue, newValue) -> showServiceDetails(newValue));
            //rellena tabla servicios
            List<ServicioJson> serviciosL = ClientRest.getServicios();
            if (serviciosL != null) {
                serviciosL.stream().forEach((ser) -> {
                    serviciosData.add(new Servicio(ser.getIdservicio(), ser.getNombre(), ser.getDescripcion(),
                            ser.getCosto(), ser.getCapacidadPersona(), ser.getTipo()));
                });
                tableServicios.setItems(serviciosData);
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Sin acceso a internet");
                alert.setHeaderText("Error al conectarse a la API");
                alert.setContentText("No se puede establecer la conexion verifique internet");

                alert.showAndWait();
            }
            confirmaReserva.getItems().addAll(
                    "true",
                    "false"
            );
            confirmaReserva.setValue("false");
            tipoFactura.getItems().addAll(
                    "Factura A",
                    "Factura B",
                    "Factura C",
                    "Factura X"
            );
            tipoFactura.setValue("Factura B");
            List<EfectivoJson> efectivoL = ClientRest.getEfectivo();
            for (EfectivoJson cbEfec : efectivoL) {
                monedaCombo.getItems().add(cbEfec.getMoneda());
            }
            //monedaCombo.getValue()
            //monedaCombo.getSelectionModel().getSelectedIndex();
            //vista confirma
            columnReservaId.setCellValueFactory(cellData -> cellData.getValue().idReservaProperty());
            columnReservaServicio.setCellValueFactory(cellData -> cellData.getValue().idServicioProperty());
            columnReservaFechaIn.setCellValueFactory(cellData -> cellData.getValue().fechaInProperty());
            columnReservaHoraIn.setCellValueFactory(cellData -> cellData.getValue().horaInProperty());
            columnReservaEstado.setCellValueFactory(cellData -> cellData.getValue().estadoProperty());
            showConfirmaDetails(null);
            // Listen for selection changes and show the person details when changed.
            tableReservas.getSelectionModel().selectedItemProperty().addListener(
                    (observable, oldValue, newValue) -> showConfirmaDetails(newValue));
            //vista pago
            columnPagoIdReserva.setCellValueFactory(cellData -> cellData.getValue().idReservaProperty());
            columnPagoFechaIn.setCellValueFactory(cellData -> cellData.getValue().fechaInProperty());
            columnPagoFechaOut.setCellValueFactory(cellData -> cellData.getValue().fechaOutProperty());
            columnPagoHoraIn.setCellValueFactory(cellData -> cellData.getValue().horaInProperty());
            columnPagoHoraOut.setCellValueFactory(cellData -> cellData.getValue().horaOutProperty());
            columnPagoServicio.setCellValueFactory(cellData -> cellData.getValue().servicioNombreProperty());
            columnPagoCosto.setCellValueFactory(cellData -> cellData.getValue().servicioCostooProperty());
            //vista Gancias
            columnGananciaIdFactura.setCellValueFactory(cellData -> cellData.getValue().idFacturaProperty());
            columnGananciaMontoTotal.setCellValueFactory(cellData -> cellData.getValue().montoTotalProperty());
            columnGananciaFecha.setCellValueFactory(cellData -> cellData.getValue().fechaProperty());
            columnGananciaTipo.setCellValueFactory(cellData -> cellData.getValue().tipoProperty());
        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al conectar");
            alert.setContentText(e.getMessage());

            alert.showAndWait();
        }
    }

    //application.getHostServices().showDocument(hyperlink.getText());
    @FXML
    private void fechaGananciaAction(ActionEvent event) {
        fechaGanancia.setDisable(true);
        DateTimeFormatter sdf
                = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            if (fechaGanancia.getValue() != null) {
                facturasData.clear();
                float sumaF = 0;
                List<FacturaDatoJson> facturasL = ClientRest.getFacturasFecha(fechaGanancia.getValue().format(sdf));
                if (facturasL != null) {
                    for (FacturaDatoJson fas : facturasL) {
                        facturasData.add(new FacturaDato(
                                fas.getIdFactura(),
                                fas.getMontoTotal(),
                                fas.getFecha(),
                                fas.getTipo()
                        )
                        );
                        sumaF += fas.getMontoTotal();
                    }
                    textGananciaTotal.setText(String.valueOf(sumaF));
                    System.out.println("total:" + sumaF);
                    tableGanancia.setItems(facturasData);

                }
                //rellena tabla reservas en la vista confirmacion
                reservasData.clear();
            }
        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al realizar filtro de ganancias");
            alert.setContentText(e.getMessage());
            System.err.println(e.getMessage());
            alert.showAndWait();
        }
        fechaGanancia.setDisable(false);
    }

    @FXML
    private void buttonPagarAction(ActionEvent event) throws IOException {

        buttonPagar.setDisable(true);
        DateTimeFormatter sdf
                = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        //get current date time with Date()
        Date date = new Date();

        try {
            if (monedaCombo.getValue() != null && !"nulo".equals(monedaCombo.getValue())) {
                FacturaJson factura = new FacturaJson(0,
                        Float.valueOf(costoTotal.getText()),
                        dateFormat.format(date),
                        tipoFactura.getValue(),
                        monedaCombo.getSelectionModel().getSelectedIndex() + 1,
                        0,
                        "",
                        "",
                        0,
                        "",
                        0,
                        0
                );

                //ventana de la factura
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/tesco/vista/Factura.fxml"));
                AnchorPane parent = (AnchorPane) loader.load();//ACA ESTA EL PROBLEMA
                Stage stage = new Stage();
                Scene scene = new Scene(parent);
                stage.initStyle(StageStyle.UTILITY);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.setTitle("CRM Ecoturisamo - Factura");

                Image ico = new Image("/recursos/Icon.png");
                stage.getIcons().add(ico);
                stage.setScene(scene);
                // Set the person into the controller.
                FacturaController controller = loader.getController();
                FacturaJson facturaR = ClientRest.postPagoAdd(factura);
                if (facturaR != null) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Información");
                    alert.setHeaderText("La carga fue exitosa");
                    alert.setContentText("Se ha agragado una nuevo Pago en efectivo");

                    alert.showAndWait();
                    ObservableList<ReservaPago> reservaPagoDataFac = reservaPagoData;
                    controller.setFacturaJson(facturaR);
                    controller.setReservaPagoData(reservaPagoDataFac);
                    controller.setPersona(cliente);
                    controller.setTipoFactura(1);
                    stage.showAndWait();
                    reservaPagoData.stream().forEach((resP) -> {
                        ClientRest.getReservaPagada(resP.getIdReserva(), String.valueOf(facturaR.getIdFactura()));
                    });
                    reservaPagoData.clear();
                } else {
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialogo");
                    alert.setHeaderText("Error al subir el pago");
                    alert.setContentText("No se ha podigo agregar un nuevo pago en efectivo");

                    alert.showAndWait();
                }
                buttonPagar.setDisable(false);
                return;
            } else if (!"".equals(textCobroCBU.getText())) {
                FacturaJson factura = new FacturaJson(0,
                        Float.valueOf(costoTotal.getText()),
                        dateFormat.format(date),
                        tipoFactura.getValue(),
                        0,
                        Long.valueOf(textCobroCBU.getText()),
                        textCobroBanco.getText(),
                        textCobroTitular.getText(),
                        0,
                        "",
                        0,
                        0
                );
                //ventana de la factura
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/tesco/vista/Factura.fxml"));
                AnchorPane parent = (AnchorPane) loader.load();

                //Parent parent = FXMLLoader.load(getClass().getResource("/tesco/vista/Factura.fxml"));
                Stage stage = new Stage();
                Scene scene = new Scene(parent);
                stage.initStyle(StageStyle.UTILITY);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.setTitle("CRM Ecoturisamo - Factura");
                // Set the application icon.
                Image ico = new Image("/recursos/Icon.png");
                stage.getIcons().add(ico);
                stage.setScene(scene);
                // Set the person into the controller.
                FacturaController controller = loader.getController();
                FacturaJson facturaR = ClientRest.postPagoAdd(factura);
                if (facturaR != null) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Información");
                    alert.setHeaderText("La carga fue exitosa");
                    alert.setContentText("Se ha agragado una nuevo Pago con debito");

                    alert.showAndWait();
                    ObservableList<ReservaPago> reservaPagoDataFac = reservaPagoData;
                    controller.setFacturaJson(facturaR);
                    controller.setReservaPagoData(reservaPagoDataFac);
                    controller.setPersona(cliente);
                    controller.setTipoFactura(2);
                    stage.showAndWait();
                    reservaPagoData.stream().forEach((resP) -> {
                        ClientRest.getReservaPagada(resP.getIdReserva(), String.valueOf(facturaR.getIdFactura()));
                    });
                    reservaPagoData.clear();
                } else {
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialogo");
                    alert.setHeaderText("Error al subir el pago");
                    alert.setContentText("No se ha podigo agregar un nuevo pago con debito");

                    alert.showAndWait();
                }
                buttonPagar.setDisable(false);
                return;
            } else if (!"".equals(textCobroNumTargeta.getText())) {
                FacturaJson factura = new FacturaJson(0,
                        Float.valueOf(costoTotal.getText()),
                        dateFormat.format(date),
                        tipoFactura.getValue(),
                        0,
                        0,
                        "",
                        "",
                        Long.valueOf(textCobroNumTargeta.getText()),
                        fechaCobroVencimiento.getValue().format(sdf),
                        Integer.valueOf(textCobroCodSeg.getText()),
                        Integer.valueOf(textCobroNumCuotas.getText())
                );
                //ventana de la factura
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/tesco/vista/Factura.fxml"));
                AnchorPane parent = (AnchorPane) loader.load();

                //Parent parent = FXMLLoader.load(getClass().getResource("/tesco/vista/Factura.fxml"));
                Stage stage = new Stage();
                Scene scene = new Scene(parent);
                stage.initStyle(StageStyle.UTILITY);
                stage.initModality(Modality.WINDOW_MODAL);
                stage.setTitle("CRM Ecoturisamo - Factura");
                Image ico = new Image("/recursos/Icon.png");
                stage.getIcons().add(ico);
                stage.setScene(scene);
                // Set the person into the controller.
                FacturaController controller = loader.getController();
                FacturaJson facturaR = ClientRest.postPagoAdd(factura);
                if (facturaR != null) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Información");
                    alert.setHeaderText("La carga fue exitosa");
                    alert.setContentText("Se ha agragado una nuevo Pago con targeta de credito");

                    alert.showAndWait();
                    ObservableList<ReservaPago> reservaPagoDataFac = reservaPagoData;
                    controller.setFacturaJson(facturaR);
                    controller.setReservaPagoData(reservaPagoDataFac);
                    controller.setPersona(cliente);
                    controller.setTipoFactura(3);
                    stage.showAndWait();
                    reservaPagoData.stream().forEach((resP) -> {
                        ClientRest.getReservaPagada(resP.getIdReserva(), String.valueOf(facturaR.getIdFactura()));
                    });
                    reservaPagoData.clear();
                } else {
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialogo");
                    alert.setHeaderText("Error al subir el pago");
                    alert.setContentText("No se ha podigo agregar un nuevo pago con targeta de credito");

                    alert.showAndWait();
                }
                buttonPagar.setDisable(false);
                return;
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialogo");
                alert.setHeaderText("Error al realizar el pago y la factura");
                alert.setContentText("No se ha podigo realizar el pago, verique los campos");
                alert.showAndWait();
            }
        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al realizar el pago y la factura");
            alert.setContentText(e.getMessage());
            System.err.println(e.getMessage());
            alert.showAndWait();
        }
        buttonPagar.setDisable(false);

    }

    @FXML
    private void buttonConfirmaAction(ActionEvent event) {
        buttonConfirma.setDisable(true);
        try {
            if (ClientRest.getReservaConfirma(textConfirmaId.getText())) {
                refrescaTablas();
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Información");
                alert.setHeaderText("Se confirma exitosamente");
                alert.setContentText("Se ha confirmado la reserva");

            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialogo");
                alert.setHeaderText("Error al confirmar la reserva");
                alert.setContentText("No se ha podigo confirmar la reserva");
                alert.showAndWait();
            }
        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al confirmar la reserva");
            alert.setContentText("Los valores ingresados no son correctos en tipo");

            alert.showAndWait();
        }
        buttonConfirma.setDisable(false);
    }

    @FXML
    private void buttonReservaAddAction(ActionEvent event) {
        buttonReservaAdd.setDisable(true);
        DateTimeFormatter sdf
                = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter sdt
                = DateTimeFormatter.ofPattern("HH:mm:ss");
        try {
            if (!"".equals(textServicioReserva.getText())) {
                ReservaJson reserva = new ReservaJson(
                        fechaInReserva.getValue().format(sdf),
                        fechaOutReserva.getValue().format(sdf),
                        String.valueOf((int) horaInReserva.getValue()) + ":00:00",
                        String.valueOf((int) horaOutReserva.getValue()) + ":00:00",
                        Integer.valueOf(textCanPerReserva.getText()),
                        confirmaReserva.getValue(),
                        Long.valueOf(textClienteReserva.getText()),
                        1,
                        Integer.valueOf(textServicioReserva.getText())
                );
                if (ClientRest.postReservasAdd(reserva)) {
                    refrescaTablas();
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Información");
                    alert.setHeaderText("La carga fue exitosa");
                    alert.setContentText("Se ha agragado una nueva Reserva");

                    alert.showAndWait();
                } else {
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialogo");
                    alert.setHeaderText("Error al subir la reserva");
                    alert.setContentText("No se ha podigo agregar una nueva reserva");

                    alert.showAndWait();
                }
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialogo");
                alert.setHeaderText("Error al agregar una reserva");
                alert.setContentText("Debe completar los campos y seleccionar un servicio");
                alert.showAndWait();
            }
        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al agregar una reserva");
            alert.setContentText("Los valores ingresados no son correctos en tipo");

            alert.showAndWait();
        }
        buttonReservaAdd.setDisable(false);
    }

    @FXML
    private void buttonClienteAgregarAction(ActionEvent event) {
        buttonClienteAgregar.setDisable(true);
        try {
            if (!"".equals(textClienteDNI.getText())) {
                PersonJson cliente = new PersonJson(
                        Long.valueOf(textClienteDNI.getText()), textClienteNombre.getText(), textClienteApellido.getText(), Long.valueOf(textClienteTelefono.getText()), textClientePais.getText(), textClienteProvincia.getText(), textClienteCuidad.getText(), textClienteDireccion.getText());
                if (ClientRest.postClienteAdd(cliente)) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.setHeaderText("La carga fue exitosa");
                    alert.setContentText("Se ha agragado un nuevo cliente");

                    alert.showAndWait();
                } else {
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialogo");
                    alert.setHeaderText("Error al subir el cliente");
                    alert.setContentText("No se ha podigo agregar un nuevo cliente");

                    alert.showAndWait();
                }
            } else {
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error Dialogo");
                alert.setHeaderText("Error al agregar un cliente");
                alert.setContentText("Debe completar todos los datos del cliente");

                alert.showAndWait();
            }

        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al agregar un cliente");
            alert.setContentText("Los valores ingresados no son correctos en tipo");

            alert.showAndWait();
        }
        buttonClienteAgregar.setDisable(false);
    }

    @FXML
    private void buttonPersonDNIAction(ActionEvent event) {
        buttonPersonDNI.setDisable(true);
        personsData.clear();
        try {
            if (textPersonDNI.getText() != null) {
                System.out.println(textPersonDNI.getText());
                PersonJson perDNI = ClientRest.getPersonaDNI(Long.parseLong(textPersonDNI.getText()));
                if (perDNI != null) {
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Information Dialog");
                    alert.setHeaderText("Cliente Encontrado");
                    alert.setContentText("Se ha encontrado el cliente");

                    alert.showAndWait();
                    personsData.add(new Persona(perDNI.getDni(), perDNI.getNombre(), perDNI.getApellido(),
                            perDNI.getTelefono(), perDNI.getPais(), perDNI.getProvincia(), perDNI.getCiudad(), perDNI.getDireccion()));
                } else {
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialogo");
                    alert.setHeaderText("Error al buscar un cliente");
                    alert.setContentText("No se encontraron clientes");

                    alert.showAndWait();
                }

            } else if (textPersonNombre.getText() != null) {

            } else if (textPersonApellido.getText() != null) {

            } else {

            }
        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al buscar un cliente");
            alert.setContentText("Los valores ingresados no son correctos en tipo");

            alert.showAndWait();
        }
        tableCliente.setItems(personsData);
        buttonPersonDNI.setDisable(false);
    }

    @FXML
    private void buttonBuscaClienteLimpiaAction(ActionEvent event) {
        textPersonDNI.setText("");
        textPersonNombre.setText("");
        textPersonApellido.setText("");
    }

    private void showCustomerDetails(Persona persona) {
        if (persona != null) {
            this.cliente = persona;
            // Fill the labels with info from the person object.
            textClienteReserva.setText(persona.getDNI());
            textClienteDNI.setText(persona.getDNI());
            textClienteNombre.setText(persona.getNombre());
            textClienteApellido.setText(persona.getApellido());
            textClienteTelefono.setText(persona.getTelefono());
            textClientePais.setText(persona.getPais());
            textClienteProvincia.setText(persona.getPais());
            textClienteCuidad.setText(persona.getCiudad());
            textClienteDireccion.setText(persona.getDireccion());
            refrescaTablas();

        } else {
            // Person is null, remove all the text.
            textClienteDNI.setText("");
            textClienteNombre.setText("");
            textClienteApellido.setText("");
            textClienteTelefono.setText("");
            textClientePais.setText("Argentina");
            textClienteProvincia.setText("Chaco");
            textClienteCuidad.setText("Resistencia");
            textClienteDireccion.setText("");
        }
    }

    private void showServiceDetails(Servicio servicio) {
        if (servicio != null) {
            textServicioReserva.setText(servicio.getIdservicio());
        } else {
            textServicioReserva.setText("");
        }
    }

    private void showConfirmaDetails(Reserva reserva) {
        if (reserva != null) {
            textConfirmaId.setText(reserva.getIdReserva());
            textConfirmaFecha.setText(reserva.getFechaIn());
        } else {
            textConfirmaId.setText("");
            textConfirmaFecha.setText("");
        }
    }

    private void refrescaTablas() {
        //rellena tabla servicios para el pago
        reservaPagoData.clear();
        float suma = 0;
        List<ReservaPagoJson> reservaPagoL = ClientRest.getReservaPagoDNI(textClienteReserva.getText());
        if (reservaPagoL != null) {
            for (ReservaPagoJson res : reservaPagoL) {
                reservaPagoData.add(new ReservaPago(
                        res.getIdReserva(), res.getFechaIn(), res.getFechaOut(),
                        res.getHoraIn(), res.getHoraOut(), res.getIdServicio(),
                        res.getServicioNombre(), res.getServicioCosto()
                )
                );
                suma += res.getServicioCosto();
            }
            costoTotal.setText(String.valueOf(suma));
            System.out.println("total:" + suma);
            tableReservasPago.setItems(reservaPagoData);

        }
        //rellena tabla reservas en la vista confirmacion
        reservasData.clear();
        List<ReservaJson> reservasL = ClientRest.getReservaDNI(textClienteReserva.getText(), "false");
        if (reservasL != null) {
            reservasL.stream().forEach((res) -> {
                reservasData.add(new Reserva(
                        res.getIdReserva(), res.getFechaIn(), res.getFechaOut(),
                        res.getHoraIn(), res.getHoraOut(), res.getCantPersona(),
                        res.getEstado(), res.getDni(), res.getIdfactura(), res.getIdServicio()
                )
                );
            });
            tableReservas.setItems(reservasData);
        }

    }

    private Object getStyleClass() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
