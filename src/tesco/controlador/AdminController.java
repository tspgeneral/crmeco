/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.controlador;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import tesco.ApiRest.ClientRest;
import tesco.ApiRest.Model.Reserva;
import tesco.ApiRest.Model.ReservaJson;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import tesco.ApiRest.Model.FacturaDato;
import tesco.ApiRest.Model.FacturaDatoJson;
import tesco.ApiRest.Model.FacturaMetodoPago;
import tesco.ApiRest.Model.JsonFacturaMetodoPago;
import tesco.ApiRest.Model.Servicio;
import tesco.ApiRest.Model.ServicioJson;

/**
 * FXML Controller class
 *
 * @author tesco012
 */
public class AdminController implements Initializable {

    private static Alert alert;
    private ObservableList<Reserva> reservasData = FXCollections.observableArrayList();
    private ObservableList<FacturaMetodoPago> facturaMetodoPagoData = FXCollections.observableArrayList();
    private ObservableList<Servicio> serviciosData = FXCollections.observableArrayList();
    List<ReservaJson> reservasL;
    List<JsonFacturaMetodoPago> facturasL;
    //servicios
    @FXML
    private TableView<Servicio> tableServicios;
    @FXML
    private TableColumn<Servicio, String> columnIdservicioS;
    @FXML
    private TableColumn<Servicio, String> columnNombreS;
    @FXML
    private TableColumn<Servicio, String> columnDescripcionS;
    @FXML
    private TableColumn<Servicio, String> columnCostoS;
    @FXML
    private TableColumn<Servicio, String> columnCapacidadPersonaS;
    @FXML
    private TableColumn<Servicio, String> columnTipoS;

    //ganancias
    @FXML
    private TableView<FacturaMetodoPago> tableGancias;
    @FXML
    private TableColumn<FacturaMetodoPago, String> columnIdFactura;
    @FXML
    private TableColumn<FacturaMetodoPago, String> columnMontoTotal;
    @FXML
    private TableColumn<FacturaMetodoPago, String> columnFecha;
    @FXML
    private TableColumn<FacturaMetodoPago, String> columnTipo;
    @FXML
    private TableColumn<FacturaMetodoPago, String> columnMetodoPago;

    //Reservas
    @FXML
    private TableView<Reserva> tableReservas;
    @FXML
    private TableColumn<Reserva, String> columnReservaId;
    @FXML
    private TableColumn<Reserva, String> columnReservaServicio;
    @FXML
    private TableColumn<Reserva, String> columnReservaFechaIn;
    @FXML
    private TableColumn<Reserva, String> columnReservaHoraIn;
    @FXML
    private TableColumn<Reserva, String> columnReservaEstado;
    @FXML
    private TableColumn<Reserva, String> columnReservaDni;
    @FXML
    private TableColumn<Reserva, String> columnReservaCantPersona;
    @FXML
    private TextField textServicioReserva;
    @FXML
    private Button buttonFiltraReserva;
    @FXML
    private Button buttonFiltraFactura;
    @FXML
    private TextField textGananciaTotal;
    @FXML
    private TextField textFiltraDni;
    @FXML
    private ComboBox<String> combMetodoPago;
    @FXML
    private DatePicker dtFechaGanancia;

    @FXML
    private void buttonFiltraFacturaAction(ActionEvent event) {
        buttonFiltraReserva.setDisable(true);
        if (combMetodoPago.getSelectionModel().getSelectedIndex() != 0) {
            List<JsonFacturaMetodoPago> facturasLMP = facturasL.stream()
                    .filter(t -> t.getMetodoPago() == combMetodoPago.getSelectionModel().getSelectedIndex())
                    .collect(Collectors.toList());
            facturaMetodoPagoData.clear();
            float sumaF = 0;
            for (JsonFacturaMetodoPago fac : facturasLMP) {
                facturaMetodoPagoData.add(new FacturaMetodoPago(fac.getIdFactura(), fac.getMontoTotal(), fac.getFecha(),
                        fac.getTipo(), fac.getMetodoPago()));
                sumaF += fac.getMontoTotal();
            }
            textGananciaTotal.setText(String.valueOf(sumaF));
            tableGancias.setItems(facturaMetodoPagoData);
        }
        DateTimeFormatter sdf
                = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        try {
            if (dtFechaGanancia.getValue() != null) {
                List<JsonFacturaMetodoPago> facturasLF = facturasL.stream()
                        .filter(t -> t.getFecha().equals(dtFechaGanancia.getValue().format(sdf)))
                        .collect(Collectors.toList());

                facturaMetodoPagoData.clear();
                float sumaF = 0;
                for (JsonFacturaMetodoPago fac : facturasLF) {
                    facturaMetodoPagoData.add(new FacturaMetodoPago(fac.getIdFactura(), fac.getMontoTotal(), fac.getFecha(),
                            fac.getTipo(), fac.getMetodoPago()));
                    sumaF += fac.getMontoTotal();
                }
                textGananciaTotal.setText(String.valueOf(sumaF));
                tableGancias.setItems(facturaMetodoPagoData);
            }
        } catch (Exception e) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialogo");
            alert.setHeaderText("Error al realizar filtro de ganancias");
            alert.setContentText(e.getMessage());
            System.err.println(e.getMessage());
            alert.showAndWait();
        }

        buttonFiltraReserva.setDisable(false);
    }

    @FXML
    private void buttonFiltraReservaAction(ActionEvent event) {
        buttonFiltraFactura.setDisable(true);
        if (!"".equals(textServicioReserva.getText())) {
            //filtra por el servicio.
            List<ReservaJson> reservaLT = reservasL.stream()
                    .filter(t -> t.getIdServicio() == Integer.parseInt(textServicioReserva.getText()))
                    .collect(Collectors.toList());
            reservasData.clear();
            reservaLT.stream().forEach((ser) -> {
                reservasData.add(new Reserva(ser.getIdReserva(), ser.getFechaIn(), ser.getFechaOut(),
                        ser.getHoraIn(), ser.getHoraOut(), ser.getCantPersona(), ser.getEstado(), ser.getDni(), ser.getIdfactura(), ser.getIdServicio()));
            });
            tableReservas.setItems(reservasData);
            //end filtra por servicio
        } else if (!"".equals(textFiltraDni.getText())) {
            List<ReservaJson> reservaLTDni = reservasL.stream()
                    .filter(t -> t.getDni() == Long.parseLong(textFiltraDni.getText()))
                    .collect(Collectors.toList());
            reservasData.clear();
            reservaLTDni.stream().forEach((ser) -> {
                reservasData.add(new Reserva(ser.getIdReserva(), ser.getFechaIn(), ser.getFechaOut(),
                        ser.getHoraIn(), ser.getHoraOut(), ser.getCantPersona(), ser.getEstado(), ser.getDni(), ser.getIdfactura(), ser.getIdServicio()));
            });
            tableReservas.setItems(reservasData);
        }

        buttonFiltraFactura.setDisable(false);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {
        combMetodoPago.getItems().addAll(
                "Ninguna",
                "Efectivo",
                "Debito",
                "Targeta"
        );
        // Tabla de reservas
        columnReservaId.setCellValueFactory(cellData -> cellData.getValue().idReservaProperty());
        columnReservaServicio.setCellValueFactory(cellData -> cellData.getValue().idServicioProperty());
        columnReservaFechaIn.setCellValueFactory(cellData -> cellData.getValue().fechaInProperty());
        columnReservaHoraIn.setCellValueFactory(cellData -> cellData.getValue().horaInProperty());
        columnReservaEstado.setCellValueFactory(cellData -> cellData.getValue().estadoProperty());
        columnReservaDni.setCellValueFactory(cellData -> cellData.getValue().dniProperty());
        columnReservaCantPersona.setCellValueFactory(cellData -> cellData.getValue().cantPersonaProperty());

        reservasL = ClientRest.getReservas();
        if (reservasL != null) {
            reservasL.stream().forEach((ser) -> {
                reservasData.add(new Reserva(ser.getIdReserva(), ser.getFechaIn(), ser.getFechaOut(),
                        ser.getHoraIn(), ser.getHoraOut(), ser.getCantPersona(), ser.getEstado(), ser.getDni(), ser.getIdfactura(), ser.getIdServicio()));
            });
            tableReservas.setItems(reservasData);
        } else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Sin acceso a internet");
            alert.setHeaderText("Error al conectarse a la API");
            alert.setContentText("No se puede establecer la conexion verifique internet");

            alert.showAndWait();
        }

        //trabla de ganancias
        columnIdFactura.setCellValueFactory(cellData -> cellData.getValue().idFacturaProperty());
        columnMontoTotal.setCellValueFactory(cellData -> cellData.getValue().montoTotalProperty());
        columnFecha.setCellValueFactory(cellData -> cellData.getValue().fechaProperty());
        columnTipo.setCellValueFactory(cellData -> cellData.getValue().tipoProperty());
        columnMetodoPago.setCellValueFactory(cellData -> cellData.getValue().metodoPagoProperty());
        facturasL = ClientRest.getFacturasMetodoPago();
        float sumaF = 0;
        if (facturasL != null) {

            for (JsonFacturaMetodoPago fac : facturasL) {
                facturaMetodoPagoData.add(new FacturaMetodoPago(fac.getIdFactura(), fac.getMontoTotal(), fac.getFecha(),
                        fac.getTipo(), fac.getMetodoPago()));
                sumaF += fac.getMontoTotal();
            }
            textGananciaTotal.setText(String.valueOf(sumaF));
            tableGancias.setItems(facturaMetodoPagoData);
        } else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Sin acceso a internet");
            alert.setHeaderText("Error al conectarse a la API");
            alert.setContentText("No se puede establecer la conexion verifique internet");

            alert.showAndWait();
        }

        columnIdservicioS.setCellValueFactory(cellData -> cellData.getValue().idservicioProperty());
        columnNombreS.setCellValueFactory(cellData -> cellData.getValue().nombreProperty());
        columnDescripcionS.setCellValueFactory(cellData -> cellData.getValue().descripcionProperty());
        columnCostoS.setCellValueFactory(cellData -> cellData.getValue().costoProperty());
        columnCapacidadPersonaS.setCellValueFactory(cellData -> cellData.getValue().capacidadPersonaProperty());
        columnTipoS.setCellValueFactory(cellData -> cellData.getValue().tipoProperty());
        showServiceDetails(null);
        // Listen for selection changes and show the person details when changed.
        System.err.println("ACA LLEGA");        /////servicios
        tableServicios.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showServiceDetails(newValue));
        //rellena tabla servicios
        List<ServicioJson> serviciosL = ClientRest.getServicios();
        if (serviciosL != null) {
            serviciosL.stream().forEach((ser) -> {
                serviciosData.add(new Servicio(ser.getIdservicio(), ser.getNombre(), ser.getDescripcion(),
                        ser.getCosto(), ser.getCapacidadPersona(), ser.getTipo()));
            });
            tableServicios.setItems(serviciosData);
        } else {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Sin acceso a internet");
            alert.setHeaderText("Error al conectarse a la API");
            alert.setContentText("No se puede establecer la conexion verifique internet");

            alert.showAndWait();
        }
    }

    private void showServiceDetails(Servicio servicio) {
        if (servicio != null) {
            textServicioReserva.setText(servicio.getIdservicio());
        } else {
            textServicioReserva.setText("");
        }
    }

}
