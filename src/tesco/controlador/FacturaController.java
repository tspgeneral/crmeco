/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tesco.controlador;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.stage.Window;
import tesco.ApiRest.Model.FacturaJson;
import tesco.ApiRest.Model.Persona;
import tesco.ApiRest.Model.ReservaPago;

/**
 * FXML Controller class
 *
 * @author tesco012
 */
public class FacturaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    private FacturaJson facturaJson;
    private Persona persona;
    private ObservableList<ReservaPago> reservaPagoData = FXCollections.observableArrayList();
    private int tipoFactura=0; //1 -efectivo 2-bebito 3-targeta
    
    @FXML
    private TableView<ReservaPago> tableReservasPago;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoIdReserva;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoFechaIn;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoFechaOut;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoHoraIn;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoHoraOut;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoServicio;
    @FXML
    private TableColumn<ReservaPago, String> columnPagoCosto;
    
    @FXML
    private Button buttonImprimir;
    
    @FXML
    private Label labelClienteNombre;
    @FXML
    private Label labelClienteDni;
    @FXML
    private Label labelClienteTelefono;
    @FXML
    private Label labelClienteProvincia;
    @FXML
    private Label labelClienteCiudad;
    @FXML
    private Label labelClienteDireccion;
    
    @FXML
    private Label labelFacturaNumero;
    @FXML
    private Label labelFacturaDate;
    @FXML
    private Label labelFacturaTipo;
    @FXML
    private Label labelFacturaTipoPago;
    
    @FXML
    private TextArea textFacturaDetallePago;
    @FXML
    private Label textFacturaSubtotal;
    @FXML
    private Label textFacturaSubtotalNeto;
    @FXML
    private Label textFacturaTotal;

    @FXML
    private void buttonImprimirAction(ActionEvent event) {
        PrinterJob printerJob = PrinterJob.createPrinterJob();
        Scene scene = ((Node) (event.getSource())).getScene();
        Window win = ((Node) (event.getSource())).getScene().getWindow();
        if (printerJob.showPrintDialog(win) && printerJob.printPage(scene.getRoot())) {
            printerJob.endJob();
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        System.err.println("llegue  2 !!!!!!!!!!!!!!!!!!!!!!!!!!1");
        //vusta pago

        columnPagoIdReserva.setCellValueFactory(cellData -> cellData.getValue().idReservaProperty());
        columnPagoFechaIn.setCellValueFactory(cellData -> cellData.getValue().fechaInProperty());
        columnPagoFechaOut.setCellValueFactory(cellData -> cellData.getValue().fechaOutProperty());
        columnPagoHoraIn.setCellValueFactory(cellData -> cellData.getValue().horaInProperty());
        columnPagoHoraOut.setCellValueFactory(cellData -> cellData.getValue().horaOutProperty());
        columnPagoServicio.setCellValueFactory(cellData -> cellData.getValue().servicioNombreProperty());
        columnPagoCosto.setCellValueFactory(cellData -> cellData.getValue().servicioCostooProperty());

    }
    
    public void setFacturaJson(FacturaJson facturaJson) {
        this.facturaJson = facturaJson;
        labelFacturaNumero.setText(String.valueOf(facturaJson.getIdFactura()));
        labelFacturaDate.setText(facturaJson.getFecha());
        labelFacturaTipo.setText(facturaJson.getTipo());
        textFacturaSubtotal.setText(String.valueOf(facturaJson.getMontoTotal()));
        textFacturaSubtotalNeto.setText(String.valueOf(facturaJson.getMontoTotal()));
        textFacturaTotal.setText(String.valueOf(facturaJson.getMontoTotal()));
    }

    



    public void setTipoFactura(int tipoFactura) {
        this.tipoFactura = tipoFactura;
        if (tipoFactura == 1) {
            labelFacturaTipoPago.setText("Efectivo");
            textFacturaDetallePago.setText("Pago al contado cod:" + facturaJson.getIdEfectivo());

        } else if (tipoFactura == 2) {
            labelFacturaTipoPago.setText("Debito");
            textFacturaDetallePago.setText("Banco: " + facturaJson.getBanco() + " CBU: " + facturaJson.getCbu() + " Titular : " + facturaJson.getTitular());
        } else if (tipoFactura == 3) {
            labelFacturaTipoPago.setText("Targeta de Credito");
            textFacturaDetallePago.setText("Cod. Targeta: " + facturaJson.getNumTargeta() + " Cuotas : " + facturaJson.getCuotas());
        } else {
            labelFacturaTipoPago.setText("Efectivo ERR");
            textFacturaDetallePago.setText("");
        }

    }
    
    public void setReservaPagoData(ObservableList<ReservaPago> reservaPagoData) {
        this.reservaPagoData = reservaPagoData;
        tableReservasPago.setItems(reservaPagoData);
    }
    
    public void setPersona(Persona persona) {
        this.persona = persona;
        labelClienteNombre.setText(persona.getNombre() + " " + persona.getApellido());
        labelClienteDni.setText(persona.getDNI());
        labelClienteTelefono.setText(persona.getTelefono());
        labelClienteProvincia.setText(persona.getProvincia());
        labelClienteCiudad.setText(persona.getCiudad());
        labelClienteDireccion.setText(persona.getDireccion());

    }

}
